from django.urls import path
from receipts.views import (
    show_receipt,
    create_receipt,
    category_list,
    account_list,
    create_category,
    create_account,
)

urlpatterns = [
    path("", show_receipt, name="home"),
    path("categories/", category_list, name="categories"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("accounts/", account_list, name="accounts"),
    path("create/", create_receipt, name="create_receipt"),
]
